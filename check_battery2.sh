#!/bin/bash
#
# This is script is run by cron to check the battery status and
# determine whether the DPC should return to the Dock to charge
# the batteries.
#
# Due to EMI from the inductive charging system, the battery status
# cannot be reliably determined when the batteries are being
# charged. Therefore, we use a time threshold to determine when to
# leave the Dock.
#
queue_message ()
{
    msg="$1"
    q="mpc:commands"
    if redis-cli lrange $q 0 -1 | grep -q "$msg"; then
        :
    else
        redis-cli rpush $q "$msg"
    fi
}

now=$(date +%s)
MIN_CHARGE=$(redis-cli hget power min_charge)
MAX_CHARGE=$(redis-cli hget power max_charge)
CHARGE_TIME=$(redis-cli hget power charge_time)

# Time of the most recent battery status check
t=$(redis-cli hget battery_status t)
age=$((now - t))
max_age=300

if ((age >= max_age)); then
    # We are most likely charging, check the time that the
    # charging was requested. We can't know for certain when
    # the charging started because the Profiler can drift
    # out of the Dock. The best we can do is use the time that
    # the charging was requested and assume it started within
    # one profile interval.
    t_start=$(redis-cli hget events "charging:req")
    if [[ -n "$t_start" ]]; then
        t_elapsed=$((now - t_start))
        if ((t_elapsed > CHARGE_TIME)); then
            # Time to leave the Dock
            #queue_message go
            redis-cli hdel events "charging:req"
        fi
    fi
else
    charge=$(redis-cli hget battery_status rel_charge)
    current=$(redis-cli hget battery_status current)
    if ((current <= 0)); then
        if ((charge < MIN_CHARGE)); then
            # Time to enter the Dock
            queue_message dock
            redis-cli hset events "charging:req" $(date +%s)
        fi
    else
        # We are currently charging
        if ((charge >= MAX_CHARGE && charge <= 100)); then
            queue_message go
            redis-cli hdel events "charging:req"
        fi
    fi
fi
