#!/bin/bash
#
# Run DPC pre-deployment checks
#
export PATH="$PATH:/usr/local/bin"

SNDIR="$HOME/meta"
SNFILE="$SNDIR/sernums"
SENSORS=("ctd" "optode" "acm" "flntu" "flcd")

# Check the sensor serial number file to verify that sensor
# communication is working.
tfile=$(stat --format=%Y $SNFILE 2>/dev/null)
tboot=$(bc <<< "$(date +%s) - $(awk '{printf "%d\n", $1}' /proc/uptime)")

if ((tfile < tboot)); then
    echo "ERROR: sensor communication failure"
    exit 1
fi

issues=0
for name in "${SENSORS[@]}"; do
    grep -q "$name" $SNFILE || {
        echo "WARNING: potential communication problem with sensor: $name"
        ((issues++))
    }
done

# Are I2C battery comms working?
tnow=$(date +%s)
tbatt="$(redis-cli --raw hget battery_status t)"
dt=$((tnow - tbatt))
if ((dt > 70)); then
    echo "WARNING: possible battery communication problem"
fi

# Are services running?
svcs=($HOME/sv/*)
cd $HOME/service

logfile=$HOME/logs/svcheck.log
> $logfile

# Check service status
for sdir in "${svcs[@]}";do
    s="${sdir##*/}"
    [[ -h $s ]] && {
        sv start ./$s 1>> $logfile 2>&1 || {
            case "$s" in
                imm_mgr|mmpmgr)
                    echo "ERROR: service $s not starting"
                    ((issues++))
                    ;;
                *) echo "WARNING: service $s not starting" ;;
            esac
        }
   }
done

if ((issues == 0)); then
    echo "OK to deploy"
fi

exit $issues
