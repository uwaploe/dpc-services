#!/bin/bash
#

# Activate the Python virtual environment
. $HOME/.venvs/dp/bin/activate

# If the named script is not executable, pass it to Python
# otherwise run it.
prog=$(which "$1")
if [[ -z "$prog" ]]; then
    exec python "$@"
else
    exec "$@"
fi
