#!/usr/bin/env bash
#
# Save the DPC sensor serial numbers to a file.
#

SNDIR="$HOME/meta"
SNFILE="$SNDIR/sernums"
SENSORS=("ctd" "optode" "acm" "flntu" "flcd")

mkdir -p "$SNDIR"

# Stop the sensor manager
cd ~/service && sv stop ./sensormgr
# Power on all sensors
power -on "${SENSORS[@]}"

# Get DPC ID from the hostname
host="$(hostname)"
echo "DPC ${host##*-}" > "$SNFILE"

# Read the serial numbers
kermit $HOME/bin/getsn.ksc >> "$SNFILE"

# Power off the sensors
power -off "${SENSORS[@]}"
# Start the sensor manager
cd ~/service && sv start ./sensormgr
