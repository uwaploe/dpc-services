#!/bin/bash
#
# Run this script via cron to check battery charge status.
#
PATH=$HOME/bin:$PATH

queue_message ()
{
    msg="$1"
    op="${2:-rpush}"
    q="mpc:commands"
    if redis-cli lrange $q 0 -1 | grep -q "$msg"
    then
        # Message already queued
        :
    else
        redis-cli $op $q "$msg"
        echo "Queuing command for MPC: '$msg'"
    fi
}

send_to_dock ()
{
    cfgfile="$HOME/config/profiles/down.yaml"
    if [[ -f $cfgfile ]]; then
        load_profiles -device=dpc "$cfgfile"
    else
        queue_message dock lpush
    fi
}

read_vmon ()
{
    local linenum=0
    local line vmon
    coproc stdbuf -oL rdadc $HOME/config/adcfg.yaml

    while read line; do
        if ((linenum == 1)); then
            vmon=$(cut -f3 -d, <<< "$line")
            kill $COPROC_PID
            echo "$vmon"
            return
        fi
        ((linenum++))
    done <&"${COPROC[0]}"
}

CFGDIR=$HOME/config
[[ -r $CFGDIR/powercfg ]] && . $CFGDIR/powercfg

MIN_VOLTAGE="${POWER{min_voltage]}"
[[ -z "$MIN_VOLTAGE" ]] && MIN_VOLTAGE=14000

charge=$(redis-cli --raw hget battery_status rel_charge)
voltage=$(redis-cli --raw hget battery_status voltage)
current=$(redis-cli --raw hget battery_status current)

# If the battery data is not available, read the VMON ADC channel
if ((voltage == 0)); then
    voltage=$(read_vmon)
    # We need to convert from floating point volts to integer
    # millivolts. Setting the scale parameter to zero will force the
    # truncation to an integer, but it is only applied after a division
    # operation, hence we divide by 0.001 rather than multiply by 1000.
    voltage=$(echo "scale=0; $voltage/0.001" | bc)
fi

# Exit if we are already charging
((current > 0)) && exit 0

# Send a dock command to the motor control service
if ((voltage > 0 &&  voltage < MIN_VOLTAGE)); then
    redis-cli publish motor.control "dock"
    # Ensure IPS rectifier is enabled
    tsfpga dioset 8160_DIO_15 low
fi
