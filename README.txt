DPC Services
============

These services are designed to run under control of the `Runit`_ service supervisor.

.. _Runit: http://smarden.org/runit/


runsvdir-rsn
------------

Service to manage additional services running under the rsn user account. This
service needs to be installed under the system directory ``/etc/service``. All
of the remaining services are installed under ``/home/rsn/service``.

datamgr
-------

Creates a compressed tar file from each archive directory at the end of a
profile.

dataxfer
--------

Transfers data to the DSC when the Profiler docks.

syscheck
--------

Performs an automated system check at boot time.

watchdog
--------

Services the external watchdog circuit.
