#!/bin/bash
#
# Start the MPC at a later time. Accepts all time specifications
# allowed by the at(1) command.
#
export PATH=$PATH:$HOME/bin

if [ $# -lt 1 ]
then
    echo "Usage: $(basename $0) timespec" 1>&2
    exit 1
fi

LP=$HOME/bin/load_profiles

pwr=$(power.sh mpc | cut -f2 -d= 2> /dev/null)
if [[ $pwr != "off" ]]; then
    echo "The MPC must be powered-off" 1>&2
    exit 1
fi

# Remove any existing profile definitions from
# the Redis data store.
echo -n "Removing existing profile definitions ..."
redis-cli del mpc:profiles 2> /dev/null
redis-cli del mpc:commands 2> /dev/null
redis-cli --raw keys 'pattern:*' 2> /dev/null |\
    xargs -r redis-cli del > /dev/null
echo " done"

# Provide user with a list of all profile
# definition files.
pfiles=($HOME/config/profiles/*.yaml)
choices=()
declare -A map
for f in "${pfiles[@]}"; do
    b=$(basename $f .yaml)
    name="${b%_*}"
    variant="${b##*_}"
    [[ -n $variant ]] && variant="($variant)"
    title="${name//-/ } $variant"
    choices+=("$title")
    map[$title]="$f"
done
choices+=("go-to Dock")

PS3="Select profile type(#): "
select choice in "${choices[@]}"; do
    if [[ "$choice" ]];then
        f="${map[$choice]}"
        if [[ "$f" && -s "$f" ]];then
            $LP --no-go --device=dpc "$f"
            read -p "Proceed with scheduling [y/N]? " ans
            case "$ans" in
                Y|y)
                    echo "redis-cli rpush mpc:commands go > /dev/null" | at "$@"
                    ;;
                *)
                    echo "Removing profiles ..."
                    redis-cli del mpc:profiles 2> /dev/null
                    redis-cli --raw keys 'pattern:*' 2> /dev/null |\
                        xargs -r redis-cli del > /dev/null
                    ;;
            esac
        else
            # Schedule a 'dock' command for a later time
            echo "redis-cli rpush mpc:commands dock > /dev/null" | at "$@"
        fi
        break
    fi
done
