#!/bin/bash
#
# Install a crontab file to run a series of profiles on a regular
# schedule. The schedule also incorporates a weekly system reboot every
# Sunday at 23:45.
#
export PATH=$PATH:$HOME/bin

process ()
{
    local hours f ftmp

    CRONTAB=/etc/cron.d/default_profile
    f="$1"
    hours="${2:-6}"
    ftmp="/tmp/crontab.$$"
    if [[ "$f" && -s "$f" ]];then
        echo "0 */${hours} * * * rsn \$HOME/bin/load_profiles --device=dpc $f" > $ftmp
        echo "45 23 * * 0 root /sbin/reboot" >> $ftmp
        sudo cp $ftmp $CRONTAB
        rm -f $ftmp
    else
        echo "Invalid profile file: \"$f\""
    fi
}


if [[ -n "$1" ]]; then
    process "$HOME/config/profiles/$1" "$2"
else
    # Provide user with a list of all profile
    # definition files.
    pfiles=($HOME/config/profiles/*_default.yaml)
    choices=()
    declare -A map
    for f in "${pfiles[@]}"; do
        b=$(basename $f .yaml)
        name="${b%_*}"
        title="${name//-/ }"
        choices+=("$title")
        map[$title]="$f"
    done

    read -p "Enter profile interval in hours [6]: " hours
    PS3="Select profile type(#): "
    select choice in "${choices[@]}"; do
        if [[ "$choice" ]];then
            process "${map[$choice]}" "$hours"
            break
        fi
    done
fi