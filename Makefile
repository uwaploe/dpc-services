#
# Makefile to install service scripts.
#
BINDIR := $(HOME)/bin
SVCDIR := $(HOME)/sv
SYS_SVCDIR := /etc/sv

SVCS := datamgr \
	dataxfer \
        syscheck \
	firstprofile \
	watchdog
SYS_SVCS := runsvdir-rsn
PROGS := bcheck.sh flashled.sh \
         getsn.ksc check_battery.sh

.PHONY: install install-sv install-sys install-bin

all: install-sv install-bin install-sys

install-sv: $(SVCS)
	cp -a -v -t $(SVCDIR) $^

install-sys: $(SYS_SVCS)
	sudo cp -a -v -t $(SYS_SVCDIR) $^

install-bin: $(PROGS)
	install -d $(BINDIR)
	install -m 755 -t $(BINDIR) $^

install-redis: redis-runit.conf redis
	sudo cp -a -v -t $(SYS_SVCDIR) redis
	sudo cp -a -v redis-runit.conf /etc/redis
	sudo invoke-rc.d redis-server stop
	sudo update-rc.d redis-server disable 2 3 4 5
