#!/bin/bash
#
# Flash the LED
#
LED_DIO="8160_DIO_3"

blink() {
    local t_on t_off n
    t_on="$1"
    t_off="$2"
    n="$3"
    tsfpga dioset "$LED_DIO" low
    for i in $(seq 1 $n); do
        tsfpga dioset "$LED_DIO" high
        sleep "$t_on"
        tsfpga dioset "$LED_DIO" low
        sleep "$t_off"
    done
}

if [[ $# -lt 3 ]]; then
    echo "Usage: ${0##*/} on_secs off_secs repeat" 1>&2
    exit 1
fi

blink "$@"
