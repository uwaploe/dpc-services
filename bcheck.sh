#!/usr/bin/env bash
#
# Check the battery status via the Battery Aggregator Board (BAB)
#
export PATH=$PATH:/usr/local/bin:$HOME/bin

if babcheck; then
    :
else
    # Check failed, reset the BAB and try again
    tsfpga dioset 8160_DIO_1 low
    sleep 1
    tsfpga dioset 8160_DIO_1 high
    babcheck
fi
