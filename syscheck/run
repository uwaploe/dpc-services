#!/bin/bash
#
# Run DPC pre-deployment checks
#
export PATH="$PATH:/usr/local/bin:$HOME/bin"

LED_DIO="8160_DIO_3"

blink() {
    local t_on t_off n
    t_on="$1"
    t_off="$2"
    n="$3"
    tsfpga dioset "$LED_DIO" low
    for i in $(seq 1 $n); do
        tsfpga dioset "$LED_DIO" high
        sleep "$t_on"
        tsfpga dioset "$LED_DIO" low
        sleep "$t_off"
    done
}


# Minimum deployment voltage in mV
MIN_VOLTAGE=14000

SNDIR="$HOME/meta"
SNFILE="$SNDIR/sernums"
SENSORS=("ctd" "optode" "acm" "flntu" "flcd")

# Ensure LED is off
tsfpga dioset "$LED_DIO" low

# Log the sensor serial numbers to verify operation.
mkdir -p "$SNDIR"
tmpfile="$(mktemp)"
trap "rm -f $tmpfile" EXIT

# Power on all sensors
power -on "${SENSORS[@]}"

# Get DPC ID from the hostname
host="$(hostname)"
echo "DPC ${host##*-}" > "$tmpfile"

# Read the serial numbers
kermit $HOME/bin/getsn.ksc >> "$tmpfile"

# Power off the sensors
power -off "${SENSORS[@]}"

# Store the serial number file.
mv "$tmpfile" "$SNFILE"

# Were any sensor problems detected?
issues=0
for name in "${SENSORS[@]}"; do
    # If communication failed, the sensor entry will be missing from
    # the serial-number file.
    grep -q "$name" $SNFILE || {
        echo "ERROR: potential communication problem with sensor: $name"
        ((issues++))
    }
done

# Are services running?
cd $HOME/service
svcs=(*)

# Check the status of all services
for sdir in "${svcs[@]}"; do
    s="${sdir##*/}"
    # Skip this service
    [[ $s = "syscheck" ]] && continue
    [[ -h $s ]] && {
        sv start ./$s 2>&1 || {
            case "$s" in
                dpcimm|mmpmgr|motorsvc|watchdog)
                    # These are critical services
                    echo "ERROR: service $s not starting"
                    ((issues++))
                    ;;
                *) echo "WARNING: service $s not starting" ;;
            esac
        }
   }
done

# Wait up to 20 seconds for another battery sample to be acquired
tbatt0="$(redis-cli --raw hget battery_status t)"
tbatt1=$tbatt0
count=0
until ((tbatt1 > tbatt0)); do
    ((count++))
    ((count > 20)) && break
    sleep 1
    tbatt1="$(redis-cli --raw hget battery_status t)"
done

# Are battery comms working?
tnow=$(date +%s)
dt=$((tnow - tbatt1))
if ((dt > 60)); then
    echo "WARNING: possible battery communication problem"
fi

# Check battery voltage even if battery comms are suspect
voltage=$(redis-cli --raw hget battery_status voltage)
if ((voltage > 0 && voltage < MIN_VOLTAGE)); then
    echo "ERROR: low battery; $voltage mV"
    ((issues++))
fi

if ((issues == 0)); then
    echo "OK to deploy"
    # Flash the LED forever to signal that the vehicle is
    # OK to deploy
    trap "tsfpga dioset "$LED_DIO" low;exit 1" INT TERM QUIT
    while true; do
        blink 0.25 0.25 10
        sleep 5
    done
fi

exit 0
