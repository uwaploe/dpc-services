#!/bin/bash
#
# This script is part of the automated system check process
# on the DPC. It is run at start-up by cron and executes the
# following tasks.
#
# - Removes the sensor serial number file
# - Waits for a new serial number file to be created (this
#   signals that the Sensor Manager service has started).
# - Starts the syscheck service
#
export PATH=$PATH:/usr/local/bin:$HOME/bin

SNDIR="$HOME/meta"
SNFILE="$SNDIR/sernums"
SVCDIR=$HOME/service/syscheck
RUNCTL="$SVCDIR/supervise/control"
LOGCTL="$SVCDIR/log/supervise/control"

[[ -e $RUNCTL ]] || {
    echo "System check service not available" 1>&2
    exit 1
}

rm -f "$SNFILE"
until [[ -e $SNFILE ]]; do
    sleep 1
done

cd $HOME/service
sv once ./syscheck
